<?php

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_Transfer extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {

        $db_source = "ushauri";
        $this->master_facility($db_source);
        $mfl_code = '12345';

        $this->condition($db_source);
        $this->consituency($db_source);
        $this->sub_county($db_source);
        $this->gender($db_source);
        $this->groups($db_source);
        $this->language($db_source);
        $this->message_types($db_source);
        $this->marital_status($db_source);
        $this->module($db_source);
        $this->partner_facility($db_source);
        $this->time($db_source);
        $this->role($db_source);
        $this->users($db_source);
        $this->sys_logs($db_source);
        $this->usr_outgoing($db_source);
        $this->incoming($db_source);
        $this->responses($db_source);
//        $this->clients($db_source, $mfl_code);
//        $this->appointment_types($db_source);
//        $this->appointments($db_source, $mfl_code);
//        $this->clnt_outgoing_msgs($db_source, $mfl_code);
    }

    public function master_facility($db_source = null) {

        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_master_facility WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_master_facility where id > $last_insert_id ")->result();
                foreach ($get_partner_facility as $value) {
                    $code = $value->code;
                    $id = $value->id;
                    $name = $value->name;
                    $reg_number = $value->reg_number;
                    $keph_level = $value->keph_level;
                    $facility_type = $value->facility_type;
                    $owner = $value->owner;
                    $regulatory_body = $value->regulatory_body;
                    $beds = $value->beds;
                    $cots = $value->cots;
                    $is_approved = $value->county_id;
                    $reason = $value->consituency_id;
                    $Sub_County_ID = $value->Sub_County_ID;
                    $Ward_id = $value->Ward_id;
                    $operational_status = $value->operational_status;
                    $Open_whole_day = $value->Open_whole_day;
                    $Open_public_holidays = $value->Open_public_holidays;
                    $Open_weekends = $value->Open_weekends;
                    $Open_late_night = $value->Open_late_night;
                    $Service_names = $value->Service_names;
                    $Approved = $value->Approved;
                    $Public_visible = $value->Public_visible;
                    $Closed = $value->Closed;
                    $assigned = $value->assigned;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $lat = $value->lat;
                    $lng = $value->lng;
                    $county_id = $value->county_id;
                    $consituency_id = $value->consituency_id;

                    $check_existence = $this->db->query("Select * from tbl_master_facility where code like '%$code%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'code' => $code,
                            'ushauri_id' => $id,
                            'name' => $name,
                            'reg_number' => $reg_number,
                            'keph_level' => $keph_level,
                            'facility_type' => $facility_type,
                            'owner' => $owner,
                            'regulatory_body' => $regulatory_body,
                            'beds' => $beds,
                            'cots' => $cots,
                            'county_id' => $county_id,
                            'consituency_id' => $consituency_id,
                            'Sub_County_ID' => $Sub_County_ID,
                            'Ward_id' => $Ward_id,
                            'operational_status' => $operational_status,
                            'Open_whole_day' => $Open_whole_day,
                            'Open_public_holidays' => $Open_public_holidays,
                            'Open_weekends' => $Open_weekends,
                            'Open_late_night' => $Open_late_night,
                            'Service_names' => $Service_names,
                            'Approved' => $Approved,
                            'Public_visible' => $Public_visible,
                            'assigned' => $assigned,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'lat' => $lat,
                            'lng' => $lng,
                            'Closed' => $Closed,
                            'db_source' => $db_source
                        );

                        $this->db->insert('master_facility', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_master_facility")->result();
            foreach ($get_partner_facility as $value) {
                $code = $value->code;
                $id = $value->id;
                $name = $value->name;
                $reg_number = $value->reg_number;
                $keph_level = $value->keph_level;
                $facility_type = $value->facility_type;
                $owner = $value->owner;
                $regulatory_body = $value->regulatory_body;
                $beds = $value->beds;
                $cots = $value->cots;
                $is_approved = $value->county_id;
                $reason = $value->consituency_id;
                $Sub_County_ID = $value->Sub_County_ID;
                $Ward_id = $value->Ward_id;
                $operational_status = $value->operational_status;
                $Open_whole_day = $value->Open_whole_day;
                $Open_public_holidays = $value->Open_public_holidays;
                $Open_weekends = $value->Open_weekends;
                $Open_late_night = $value->Open_late_night;
                $Service_names = $value->Service_names;
                $Approved = $value->Approved;
                $Public_visible = $value->Public_visible;
                $Closed = $value->Closed;
                $assigned = $value->assigned;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $lat = $value->lat;
                $lng = $value->lng;
                $county_id = $value->county_id;
                $consituency_id = $value->consituency_id;

                $check_existence = $this->db->query("Select * from tbl_master_facility where code like '%$code%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'code' => $code,
                        'ushauri_id' => $id,
                        'name' => $name,
                        'reg_number' => $reg_number,
                        'keph_level' => $keph_level,
                        'facility_type' => $facility_type,
                        'owner' => $owner,
                        'regulatory_body' => $regulatory_body,
                        'beds' => $beds,
                        'cots' => $cots,
                        'county_id' => $county_id,
                        'consituency_id' => $consituency_id,
                        'Sub_County_ID' => $Sub_County_ID,
                        'Ward_id' => $Ward_id,
                        'operational_status' => $operational_status,
                        'Open_whole_day' => $Open_whole_day,
                        'Open_public_holidays' => $Open_public_holidays,
                        'Open_weekends' => $Open_weekends,
                        'Open_late_night' => $Open_late_night,
                        'Service_names' => $Service_names,
                        'Approved' => $Approved,
                        'Public_visible' => $Public_visible,
                        'assigned' => $assigned,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'lat' => $lat,
                        'lng' => $lng,
                        'Closed' => $Closed,
                        'db_source' => $db_source
                    );

                    $this->db->insert('master_facility', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    function escape_output($string) {
        $newString = str_replace('\r\n', '<br/>', $string);
        $newString = str_replace('\n\r', '<br/>', $newString);
        $newString = str_replace('\r', '<br/>', $newString);
        $newString = str_replace('\n', '<br/>', $newString);
        $newString = str_replace('\'', '', $newString);
        return $newString;
    }

    public function consituency($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_consituency WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_consituency")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $name = $value->name;
                    $name = $this->escape_output($name);
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_consituency where name like '%$name%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'name' => $name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_consituency', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_consituency")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $name = $value->name;
                $name = $this->escape_output($name);
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_consituency where name like '%$name%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'name' => $name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_consituency', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function sub_county($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_sub_county WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }
                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_sub_county where id > $last_insert_id ")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $name = $value->name;
                    $name = $this->escape_output($name);
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_sub_county where name like '%$name%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'name' => $name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_sub_county', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_sub_county")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $name = $value->name;
                $name = $this->escape_output($name);
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_sub_county where name like '%$name%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'name' => $name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_sub_county', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function gender($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_gender WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_gender where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $name = $value->name;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_gender where name like '%$name%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'name' => $name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_gender', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_gender")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $name = $value->name;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_gender where name like '%$name%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'name' => $name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_gender', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function condition($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_condition WHERE ushauri_id IS NOT NULL ");
        if ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_condition where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $name = $value->name;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_condition where name like '%$name%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'name' => $name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_condition', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_condition")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $name = $value->name;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_condition where name like '%$name%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'name' => $name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_condition', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function groups($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_groups WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }
                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_groups where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $name = $value->name;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_groups where name like '%$name%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'name' => $name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_groups', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_groups")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $name = $value->name;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_groups where name like '%$name%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'name' => $name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_groups', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function language($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_language WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }


                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_language where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $name = $value->name;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_language where name like '%$name%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                    } else {




                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'name' => $name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_language', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_language")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $name = $value->name;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_language where name like '%$name%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                } else {



                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'name' => $name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_language', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function message_types($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_message_types WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_message_types where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $name = $value->name;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_message_types where name like '%$name%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'name' => $name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'ushauri_id' => $id,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_message_types', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_message_types")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $name = $value->name;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_message_types where name like '%$name%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'name' => $name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'ushauri_id' => $id,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_message_types', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function marital_status($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_marital_status WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_marital_status where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $marital = $value->marital;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_marital_status where marital like '%$marital%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'marital' => $marital,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('marital_status', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_marital_status")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $marital = $value->marital;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_marital_status where marital like '%$marital%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'marital' => $marital,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('marital_status', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function module($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_module WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_module where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $module = $value->module;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;
                    $level = $value->level;
                    $order = $value->order;
                    $controller = $value->controller;
                    $function = $value->function;
                    $status = $value->status;
                    $description = $value->description;
                    $span_class = $value->span_class;
                    $icon_class = $value->icon_class;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $partner_id = $value->partner_id;


                    $check_existence = $this->db->query("Select * from tbl_module where module like '%$module%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'module' => $module,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'level' => $level,
                            'order' => $order,
                            'controller' => $controller,
                            'function' => $function,
                            'description' => $description,
                            'span_class' => $span_class,
                            'icon_class' => $icon_class,
                            'partner_id' => $partner_id,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_module', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_module")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $module = $value->module;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;
                $level = $value->level;
                $order = $value->order;
                $controller = $value->controller;
                $function = $value->function;
                $status = $value->status;
                $description = $value->description;
                $span_class = $value->span_class;
                $icon_class = $value->icon_class;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $partner_id = $value->partner_id;


                $check_existence = $this->db->query("Select * from tbl_module where module like '%$module%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'module' => $module,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'level' => $level,
                        'order' => $order,
                        'controller' => $controller,
                        'function' => $function,
                        'description' => $description,
                        'span_class' => $span_class,
                        'icon_class' => $icon_class,
                        'partner_id' => $partner_id,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_module', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function partner_facility($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_partner_facility WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }


                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_partner_facility where id > $last_insert_id ")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $mfl_code = $value->mfl_code;
                    $coutny_id = $value->county_id;
                    $sub_county_id = $value->sub_county_id;
                    $is_approved = $value->is_approved;
                    $reason = $value->reason;
                    $avg_clients = $value->avg_clients;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_partner_facility where mfl_code like '%$mfl_code%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'mfl_code' => $mfl_code,
                            'county_id' => $coutny_id,
                            'sub_county_id' => $sub_county_id,
                            'is_approved' => $is_approved,
                            'reason' => $reason,
                            'avg_clients' => $avg_clients,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_partner_facility', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_partner_facility")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $mfl_code = $value->mfl_code;
                $coutny_id = $value->county_id;
                $sub_county_id = $value->sub_county_id;
                $is_approved = $value->is_approved;
                $reason = $value->reason;
                $avg_clients = $value->avg_clients;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_partner_facility where mfl_code like '%$mfl_code%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'mfl_code' => $mfl_code,
                        'county_id' => $coutny_id,
                        'sub_county_id' => $sub_county_id,
                        'is_approved' => $is_approved,
                        'reason' => $reason,
                        'avg_clients' => $avg_clients,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_partner_facility', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function time($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_time WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_time where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $name = $value->name;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_time where name like '%$name%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'name' => $name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_time', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_time")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $name = $value->name;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_time where name like '%$name%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'name' => $name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_time', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function role($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_role WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_role where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $name = $value->name;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query("Select * from tbl_role where name like '%$name%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'name' => $name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_role', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_role")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $name = $value->name;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query("Select * from tbl_role where name like '%$name%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'name' => $name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_role', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function sys_logs($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_sys_logs WHERE ushauri_id IS NOT NULL ");
        $last_insert_id = $get_last_insert_id->num_rows();
        $max_id_result = $get_last_insert_id->result();
        foreach ($max_id_result as $value) {
            $max_last_insert_id = $value->ushauri_id;
            if (empty($max_last_insert_id)) {
                echo "Our Maximum last inserted id => " . $max_last_insert_id . " end <br>";




                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_sys_logs LIMIT 10")->result();

                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $user_id = $value->user_id;
                    $description = $value->description;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;
                    echo "Select * from tbl_users where ushauri_id='$user_id' <br>";
                    $get_ushauri_user_id = $this->db->query("Select * from tbl_users where ushauri_id='$user_id'");
                    if ($get_ushauri_user_id->num_rows() > 0) {
                        foreach ($get_ushauri_user_id->result() as $value) {
                            $new_user_id = $value->id;
                            $ushauri_id = $value->ushauri_id;

                            echo 'Ushauri ID =>' . $ushauri_id . ' and   Merged User ID => ' . $new_user_id . '<br>';


                            $this->db->trans_start();


                            $data = array(
                                'ushauri_id' => $id,
                                'user_id' => $new_user_id,
                                'description' => $description,
                                'status' => $status,
                                'created_by' => $created_by,
                                'updated_by' => $updated_by,
                                'created_at' => $created_at,
                                'updated_at' => $updated_at,
                                'db_source' => $db_source
                            );

                            $this->db->insert('tbl_sys_logs', $data);

                            $this->db->trans_complete();
                            if ($this->db->trans_status() === FALSE) {
                                
                            } else {
                                
                            }
                        }
                    } else {


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'user_id' => $user_id,
                            'description' => $description,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_sys_logs', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            } else {
                echo "Max last insert id => " . $max_last_insert_id . "<br>";




                IF ($last_insert_id > 0) {

                    foreach ($get_last_insert_id->result() as $value) {
                        $last_insert_id = $value->ushauri_id;
                        if (empty($last_insert_id)) {
                            $last_insert_id .= '0';
                        } else {
                            
                        }

                        $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                        $get_partner_facility = $ushauri_db->query("Select * from tbl_sys_logs where id >= $last_insert_id ")->result();


                        foreach ($get_partner_facility as $value) {
                            $status = $value->status;
                            $id = $value->id;
                            $user_id = $value->user_id;
                            $description = $value->description;
                            $created_by = $value->created_by;
                            $updated_by = $value->updated_by;
                            $created_at = $value->created_at;
                            $updated_at = $value->updated_at;
                            $get_ushauri_user_id = $this->db->query("Select * from tbl_users where ushauri_id='$user_id'");
                            if ($get_ushauri_user_id->num_rows() > 0) {
                                foreach ($get_ushauri_user_id->result() as $value) {
                                    $new_user_id = $value->id;
                                    $ushauri_id = $value->ushauri_id;

                                    echo 'Ushauri ID =>' . $ushauri_id . ' and   Merged User ID => ' . $new_user_id . '<br>';


                                    $this->db->trans_start();


                                    $data = array(
                                        'ushauri_id' => $id,
                                        'user_id' => $new_user_id,
                                        'description' => $description,
                                        'status' => $status,
                                        'created_by' => $created_by,
                                        'updated_by' => $updated_by,
                                        'created_at' => $created_at,
                                        'updated_at' => $updated_at,
                                        'db_source' => $db_source
                                    );

                                    $this->db->insert('tbl_sys_logs', $data);

                                    $this->db->trans_complete();
                                    if ($this->db->trans_status() === FALSE) {
                                        
                                    } else {
                                        
                                    }
                                }
                            } else {


                                $this->db->trans_start();


                                $data = array(
                                    'ushauri_id' => $id,
                                    'user_id' => $user_id,
                                    'description' => $description,
                                    'status' => $status,
                                    'created_by' => $created_by,
                                    'updated_by' => $updated_by,
                                    'created_at' => $created_at,
                                    'updated_at' => $updated_at,
                                    'db_source' => $db_source
                                );

                                $this->db->insert('tbl_sys_logs', $data);

                                $this->db->trans_complete();
                                if ($this->db->trans_status() === FALSE) {
                                    
                                } else {
                                    
                                }
                            }
                        }
                    }
                } else {


                    $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                    $get_partner_facility = $ushauri_db->query("Select * from tbl_sys_logs LIMIT 100")->result();

                    foreach ($get_partner_facility as $value) {
                        $status = $value->status;
                        $id = $value->id;
                        $user_id = $value->user_id;
                        $description = $value->description;
                        $created_by = $value->created_by;
                        $updated_by = $value->updated_by;
                        $created_at = $value->created_at;
                        $updated_at = $value->updated_at;

                        $get_ushauri_user_id = $this->db->query("Select * from tbl_users where ushauri_id='$user_id'");
                        if ($get_ushauri_user_id->num_rows() > 0) {
                            foreach ($get_ushauri_user_id->result() as $value) {
                                $new_user_id = $value->id;
                                $ushauri_id = $value->ushauri_id;

                                echo 'Ushauri ID =>' . $ushauri_id . ' and   Merged User ID => ' . $new_user_id . '<br>';


                                $this->db->trans_start();


                                $data = array(
                                    'ushauri_id' => $id,
                                    'user_id' => $new_user_id,
                                    'description' => $description,
                                    'status' => $status,
                                    'created_by' => $created_by,
                                    'updated_by' => $updated_by,
                                    'created_at' => $created_at,
                                    'updated_at' => $updated_at,
                                    'db_source' => $db_source
                                );

                                $this->db->insert('tbl_sys_logs', $data);

                                $this->db->trans_complete();
                                if ($this->db->trans_status() === FALSE) {
                                    
                                } else {
                                    
                                }
                            }
                        } else {


                            $this->db->trans_start();


                            $data = array(
                                'ushauri_id' => $id,
                                'user_id' => $new_user_id,
                                'description' => $description,
                                'status' => $status,
                                'created_by' => $created_by,
                                'updated_by' => $updated_by,
                                'created_at' => $created_at,
                                'updated_at' => $updated_at,
                                'db_source' => $db_source
                            );

                            $this->db->insert('tbl_sys_logs', $data);

                            $this->db->trans_complete();
                            if ($this->db->trans_status() === FALSE) {
                                
                            } else {
                                
                            }
                        }
                    }
                }
            }
        }
    }

    public function usr_outgoing($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_usr_outgoing WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }


                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_usr_outgoing where id > $last_insert_id ")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $clnt_usr_id = $value->clnt_usr_id;
                    $recepient_type = $value->recepient_type;
                    $is_deleted = $value->is_deleted;
                    $destination = $value->destination;
                    $source = $value->source;
                    $msg = $value->msg;
                    $responded = $value->responded;
                    $message_type_id = $value->message_type_id;
                    $content_id = $value->content_id;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $get_ushauri_user_id = $this->db->query("Select * from tbl_users where ushauri_id='$clnt_usr_id'")->result();
                    foreach ($get_ushauri_user_id as $value) {
                        $new_user_id = $value->id;
                        $ushauri_id = $value->ushauri_id;

                        echo 'Ushauri ID =>' . $ushauri_id . ' and   Merged User ID => ' . $new_user_id . '<br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'clnt_usr_id' => $new_user_id,
                            'recepient_type' => $recepient_type,
                            'is_deleted' => $is_deleted,
                            'destination' => $destination,
                            'source' => $source,
                            'msg' => $msg,
                            'responded' => $responded,
                            'message_type_id' => $message_type_id,
                            'content_id' => $content_id,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_usr_outgoing', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_usr_outgoing ")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $clnt_usr_id = $value->clnt_usr_id;
                $recepient_type = $value->recepient_type;
                $is_deleted = $value->is_deleted;
                $destination = $value->destination;
                $source = $value->source;
                $msg = $value->msg;
                $responded = $value->responded;
                $message_type_id = $value->message_type_id;
                $content_id = $value->content_id;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $get_ushauri_user_id = $this->db->query("Select * from tbl_users where ushauri_id='$clnt_usr_id'")->result();
                foreach ($get_ushauri_user_id as $value) {
                    $new_user_id = $value->id;
                    $ushauri_id = $value->ushauri_id;

                    echo 'Ushauri ID =>' . $ushauri_id . ' and   Merged User ID => ' . $new_user_id . '<br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'clnt_usr_id' => $new_user_id,
                        'recepient_type' => $recepient_type,
                        'is_deleted' => $is_deleted,
                        'destination' => $destination,
                        'source' => $source,
                        'msg' => $msg,
                        'responded' => $responded,
                        'message_type_id' => $message_type_id,
                        'content_id' => $content_id,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_usr_outgoing', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    public function users($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_users WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }


                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_users where id > $last_insert_id ")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $f_name = $value->f_name;
                    $m_name = $value->m_name;
                    $l_name = $value->l_name;
                    $dob = $value->dob;
                    $phone_no = $value->phone_no;
                    $e_mail = $value->e_mail;
                    $partner_id = $value->partner_id;
                    $facility_id = $value->facility_id;
                    $donor_id = $value->donor_id;
                    $password = $value->password;
                    $first_access = $value->first_access;
                    $access_level = $value->access_level;
                    $sms_enable = $value->sms_enable;
                    $county_id = $value->county_id;
                    $daily_report = $value->daily_report;
                    $weekly_report = $value->weekly_report;
                    $motmhly_report = $value->monthly_report;
                    $month3_report = $value->month3_report;
                    $month6_report = $value->month6_report;
                    $yearly_report = $value->Yearly_report;
                    $last_pass_change = $value->last_pass_change;
                    $view_client = $value->view_client;
                    $subcounty_id = $value->subcounty_id;
                    $rcv_app_list = $value->rcv_app_list;
                    $role_id = $value->role_id;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;









                    $check_existence = $this->db->query("Select * from tbl_users where phone_no like '%$phone_no%' ");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';

                        foreach ($check_existence->result() as $value) {
                            $new_user_id = $value->id;
                            $this->db->trans_start();
                            $data_update = array(
                                'ushauri_id' => $id,
                                'db_source' => 'Ushauri Old'
                            );
                            $this->db->where('id', $new_user_id);
                            $this->db->update('users', $data_update);
                            $this->db->trans_complete();
                            if ($this->db->trans_status() === FALSE) {
                                echo "User update has fialed <br>";
                            } else {
                                echo "User updated was successfull <br>";
                            }
                        }
                    }














                    $check_existence = $this->db->query("Select * from tbl_users where  e_mail like '%$e_mail%' or phone_no like '%$phone_no%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';

                        foreach ($check_existence->result() as $value) {
                            $new_user_id = $value->id;
                            $this->db->trans_start();
                            $data_update = array(
                                'ushauri_id' => $id,
                                'db_source' => 'Ushauri Old'
                            );
                            $this->db->where('id', $new_user_id);
                            $this->db->update('users', $data_update);
                            $this->db->trans_complete();
                            if ($this->db->trans_status() === FALSE) {
                                echo "User update has fialed <br>";
                            } else {
                                echo "User updated was successfull <br>";
                            }
                        }
                    } else {
                        echo 'Does not exist <br>';



                        $check_existence = $this->db->query("Select * from tbl_users where e_mail like '%$e_mail%'");
                        if ($check_existence->num_rows() > 0) {
                            //Exists
                            echo 'Exists...<br>';
                        } else {
                            echo 'Does not exist <br>';

                            if ($access_level === 'Admin') {
                                
                            } elseif ($access_level === 'Partner') {
                                //$get_partner_id = $this->db->query('Select * from tbl_partner where ')->result();
                            } elseif ($access_level === 'County') {
                                
                            } elseif ($access_level === 'Sub County') {
                                
                            } elseif ($access_level === 'Facility') {
                                
                            }
                        }

                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'f_name' => $f_name,
                            'm_name' => $m_name,
                            'l_name' => $l_name,
                            'dob' => $dob,
                            'phone_no' => $phone_no,
                            'e_mail' => $e_mail,
                            'partner_id' => $partner_id,
                            'facility_id' => $facility_id,
                            'donor_id' => $donor_id,
                            'password' => $password,
                            'first_access' => $first_access,
                            'access_level' => $access_level,
                            'sms_enable' => $sms_enable,
                            'county_id' => $county_id,
                            'daily_report' => $daily_report,
                            'weekly_report' => $weekly_report,
                            'monthly_report' => $motmhly_report,
                            'month3_report' => $month3_report,
                            'month6_report' => $month6_report,
                            'Yearly_report' => $yearly_report,
                            'last_pass_change' => $last_pass_change,
                            'view_client' => $view_client,
                            'subcounty_id' => $subcounty_id,
                            'rcv_app_list' => $rcv_app_list,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_users', $data);



                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {

            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_users")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $f_name = $value->f_name;
                $m_name = $value->m_name;
                $l_name = $value->l_name;
                $dob = $value->dob;
                $phone_no = $value->phone_no;
                $e_mail = $value->e_mail;
                $partner_id = $value->partner_id;
                $facility_id = $value->facility_id;
                $donor_id = $value->donor_id;
                $password = $value->password;
                $first_access = $value->first_access;
                $access_level = $value->access_level;
                $sms_enable = $value->sms_enable;
                $county_id = $value->county_id;
                $daily_report = $value->daily_report;
                $weekly_report = $value->weekly_report;
                $motmhly_report = $value->monthly_report;
                $month3_report = $value->month3_report;
                $month6_report = $value->month6_report;
                $yearly_report = $value->Yearly_report;
                $last_pass_change = $value->last_pass_change;
                $view_client = $value->view_client;
                $subcounty_id = $value->subcounty_id;
                $rcv_app_list = $value->rcv_app_list;
                $role_id = $value->role_id;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;



                $check_existence = $this->db->query("Select * from tbl_users where phone_no like '%$phone_no%' ");
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';

                    foreach ($check_existence->result() as $value) {
                        $new_user_id = $value->id;
                        $this->db->trans_start();
                        $data_update = array(
                            'ushauri_id' => $id,
                            'db_source' => 'Ushauri Old'
                        );
                        $this->db->where('id', $new_user_id);
                        $this->db->update('users', $data_update);
                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            echo "User update has fialed <br>";
                        } else {
                            echo "User updated was successfull <br>";
                        }
                    }
                }

                $check_existence = $this->db->query("Select * from tbl_users where  e_mail like '%$e_mail%'");
                if ($check_existence->num_rows() > 0) {
                    //Exists

                    foreach ($check_existence->result() as $value) {
                        $new_user_id = $value->id;
                        $this->db->trans_start();
                        $data_update = array(
                            'ushauri_id' => $id,
                            'db_source' => 'Ushauri Old'
                        );
                        $this->db->where('id', $new_user_id);
                        $this->db->update('users', $data_update);
                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            echo "User update has fialed <br>";
                        } else {
                            echo "User updated was successfull <br>";
                        }
                    }
                } else {
                    echo 'Does not exist <br>';



                    $check_existence = $this->db->query("Select * from tbl_users where e_mail like '%$e_mail%' or phone_no like '%$phone_no%'");
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';

                        $this->db->trans_start();
                        $data_update = array(
                            'ushauri_id' => $id,
                            'db_source' => 'Ushauri Old'
                        );
                        $this->db->where('id', $data_update);
                        $this->db->update('users', $data_update);
                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            echo "User update has fialed <br>";
                        } else {
                            echo "User updated was successfull <br>";
                        }
                    } else {
                        echo 'Does not exist <br>';

                        if ($access_level === 'Admin') {
                            
                        } elseif ($access_level === 'Partner') {
                            //$get_partner_id = $this->db->query('Select * from tbl_partner where ')->result();
                        } elseif ($access_level === 'County') {
                            
                        } elseif ($access_level === 'Sub County') {
                            
                        } elseif ($access_level === 'Facility') {
                            
                        }
                    }

                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'f_name' => $f_name,
                        'm_name' => $m_name,
                        'l_name' => $l_name,
                        'dob' => $dob,
                        'phone_no' => $phone_no,
                        'e_mail' => $e_mail,
                        'partner_id' => $partner_id,
                        'facility_id' => $facility_id,
                        'donor_id' => $donor_id,
                        'password' => $password,
                        'first_access' => $first_access,
                        'access_level' => $access_level,
                        'sms_enable' => $sms_enable,
                        'county_id' => $county_id,
                        'daily_report' => $daily_report,
                        'weekly_report' => $weekly_report,
                        'monthly_report' => $motmhly_report,
                        'month3_report' => $month3_report,
                        'month6_report' => $month6_report,
                        'Yearly_report' => $yearly_report,
                        'last_pass_change' => $last_pass_change,
                        'view_client' => $view_client,
                        'subcounty_id' => $subcounty_id,
                        'rcv_app_list' => $rcv_app_list,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_users', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    function incoming($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_incoming WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }


                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_incoming where id > $last_insert_id")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $source = $value->source;
                    $destination = $value->destination;
                    $msg = $value->msg;
                    $senttime = $value->senttime;
                    $receivedtime = $value->receivedtime;
                    $reference = $value->reference;
                    $processed = $value->processed;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;

                    $check_existence = $this->db->query('Select * from tbl_incoming where msg = "$msg" and source like "%$source%" ');
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'source' => $source,
                            'destination' => $destination,
                            'msg' => $msg,
                            'senttime' => $senttime,
                            'receivedtime' => $receivedtime,
                            'reference' => $reference,
                            'processed' => $processed,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_incoming', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_incoming")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $source = $value->source;
                $destination = $value->destination;
                $msg = $value->msg;
                $senttime = $value->senttime;
                $receivedtime = $value->receivedtime;
                $reference = $value->reference;
                $processed = $value->processed;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;

                $check_existence = $this->db->query('Select * from tbl_incoming where msg = "$msg" and source like "%$source%" ');
                if ($check_existence->num_rows() > 0) {
                    //Exists
                    echo 'Exists...<br>';
                } else {
                    echo 'Does not exist <br>';


                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'source' => $source,
                        'destination' => $destination,
                        'msg' => $msg,
                        'senttime' => $senttime,
                        'receivedtime' => $receivedtime,
                        'reference' => $reference,
                        'processed' => $processed,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_incoming', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    function responses($db_source = null) {
        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_responses WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_responses where id > $last_insert_id ")->result();
                foreach ($get_partner_facility as $value) {

                    $id = $value->id;
                    $source = $value->source;
                    $destination = $value->destination;
                    $msg = $value->msg;
                    $group_id = $value->group_id;
                    $msg_type = $value->msg_type;
                    $language_id = $value->language_id;
                    $msg_datetime = $value->msg_datetime;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;
                    $qstn_id = $value->qstn_id;
                    $user_type = $value->user_type;
                    $recognised = $value->recognised;
                    $processed = $value->processed;
                    $incoming_id = $value->incoming_id;

//            $new_incoming_id = '';

                    $new_incoming_id_existnece = $this->db->query("Select * from tbl_incoming where ushauri_id ='$incoming_id'")->result();
                    foreach ($new_incoming_id_existnece as $value) {
                        $new_incoming_id = $value->id;

                        $check_existence = $this->db->query('Select * from tbl_incoming where msg = "$msg" and source like "%$source%"');
                        if ($check_existence->num_rows() > 0) {
                            //Exists
                            echo 'Exists...<br>';
                        } else {
                            echo 'Does not exist <br>';


                            $this->db->trans_start();


                            $data = array(
                                'ushauri_id' => $id,
                                'source' => $source,
                                'destination' => $destination,
                                'msg' => $msg,
                                'group_id' => $group_id,
                                'msg_type' => $msg_type,
                                'language_id' => $language_id,
                                'processed' => $processed,
                                'created_by' => $created_by,
                                'updated_by' => $updated_by,
                                'created_at' => $created_at,
                                'updated_at' => $updated_at,
                                'msg_datetime' => $msg_datetime,
                                'qstn_id' => $qstn_id,
                                'user_type' => $user_type,
                                'recognised' => $recognised,
                                'incoming_id' => $new_incoming_id,
                                'db_source' => $db_source
                            );

                            $this->db->insert('tbl_responses', $data);

                            $this->db->trans_complete();
                            if ($this->db->trans_status() === FALSE) {
                                
                            } else {
                                
                            }
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_responses")->result();
            foreach ($get_partner_facility as $value) {

                $id = $value->id;
                $source = $value->source;
                $destination = $value->destination;
                $msg = $value->msg;
                $group_id = $value->group_id;
                $msg_type = $value->msg_type;
                $language_id = $value->language_id;
                $msg_datetime = $value->msg_datetime;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;
                $qstn_id = $value->qstn_id;
                $user_type = $value->user_type;
                $recognised = $value->recognised;
                $processed = $value->processed;
                $incoming_id = $value->incoming_id;

//            $new_incoming_id = '';

                $new_incoming_id_existnece = $this->db->query("Select * from tbl_incoming where ushauri_id ='$incoming_id'")->result();
                foreach ($new_incoming_id_existnece as $value) {
                    $new_incoming_id = $value->id;

                    $check_existence = $this->db->query('Select * from tbl_incoming where msg = "$msg" and source like "%$source%"');
                    if ($check_existence->num_rows() > 0) {
                        //Exists
                        echo 'Exists...<br>';
                    } else {
                        echo 'Does not exist <br>';


                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'source' => $source,
                            'destination' => $destination,
                            'msg' => $msg,
                            'group_id' => $group_id,
                            'msg_type' => $msg_type,
                            'language_id' => $language_id,
                            'processed' => $processed,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'msg_datetime' => $msg_datetime,
                            'qstn_id' => $qstn_id,
                            'user_type' => $user_type,
                            'recognised' => $recognised,
                            'incoming_id' => $new_incoming_id,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_responses', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        }
    }

    function clients($db_source = null, $mfl_code = null) {
        $this->db = $this->load->database('post_T4A_Ushauri', TRUE);
        // the TRUE paramater tells CI that you'd like to return the database object.

        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id "
                . " FROM tbl_client"
                . " WHERE ushauri_id IS NOT NULL and db_source='Ushauri' and mfl_code='$mfl_code'  ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }
                echo "Last Insert ID => " . $last_insert_id;

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $get_partner_facility = $ushauri_db->query("Select * from tbl_client where id > $last_insert_id and mfl_code='$mfl_code'  ")->result();
                foreach ($get_partner_facility as $value) {
                    $status = $value->status;
                    $id = $value->id;
                    $group_id = $value->group_id;
                    $language_id = $value->language_id;
                    $facility_id = $value->facility_id;
                    $clinic_number = $value->clinic_number;
                    $f_name = $value->f_name;
                    $m_name = $value->m_name;
                    $l_name = $value->l_name;
                    $created_by = $value->created_by;
                    $updated_by = $value->updated_by;
                    $created_at = $value->created_at;
                    $updated_at = $value->updated_at;
                    $dob = $value->dob;
                    $client_status = $value->client_status;
                    $txt_frequency = $value->txt_frequency;
                    $txt_time = $value->txt_time;
                    $phone_no = $value->phone_no;
                    $alt_phone_no = $value->alt_phone_no;
                    $shared_no_name = $value->shared_no_name;
                    $smsenable = $value->smsenable;
                    $partner_id = $value->partner_id;
                    $mfl_code = $value->mfl_code;
                    $gender = $value->gender;
                    $marital = $value->marital;
                    $enrollment_date = $value->enrollment_date;
                    $art_date = $value->art_date;
                    $wellness_enable = $value->wellness_enable;
                    $motivational_enable = $value->motivational_enable;
                    $client_type = $value->client_type;
                    $prev_clinic = $value->prev_clinic;
                    $transfer_date = $value->transfer_date;
                    $entry_point = $value->entry_point;
                    $welcome_sent = $value->welcome_sent;
                    $stable = $value->stable;
                    $physical_address = $value->physical_address;
                    $consent_date = $value->consent_date;


                    $new_group_id = '';
                    $new_language_id = '';
                    $new_facility_id = '';
                    $new_txt_time = '';
                    $new_partner_id = '';
                    $new_gender_id = '';
                    $new_marital_id = '';
                    $new_created_by = '';
                    $new_updated_by = '';

                    $group_existence = $this->db->query("Select * from tbl_groups where ushauri_id = '$group_id'")->result();
                    foreach ($group_existence as $value) {
                        $new_group_id .= $value->id;
                    }
                    if ($language_id == 5 or $language_id == 3) {
                        $new_language_id = '3';
                    } else {
                        $language_existence = $this->db->query("Select * from tbl_language where ushauri_id = '$language_id'")->result();
                        foreach ($language_existence as $value) {
                            $new_language_id .= $value->id;
                        }
                    }

                    $txt_time_existence = $this->db->query("Select * from tbl_time where ushauri_id = '$txt_time'")->result();
                    foreach ($txt_time_existence as $value) {
                        $new_txt_time .= $value->id;
                    }
                    $partner_existence = $this->db->query("Select * from tbl_partner where ushauri_id = '$partner_id'")->result();
                    foreach ($partner_existence as $value) {
                        $new_partner_id .= $value->id;
                    }
                    $gender_existence = $this->db->query("Select * from tbl_gender where ushauri_id = '$gender'")->result();
                    foreach ($group_existence as $value) {
                        $new_gender_id .= $value->id;
                    }
                    $marital_existence = $this->db->query("Select * from tbl_marital_status where ushauri_id = '$marital'")->result();
                    foreach ($marital_existence as $value) {
                        $new_marital_id .= $value->id;
                    }
                    $created_by_existence = $this->db->query("Select * from tbl_users where ushauri_id = '$created_by'")->result();
                    foreach ($created_by_existence as $value) {
                        $new_created_by .= $value->id;
                    }
                    $updated_by_existence = $this->db->query("Select * from tbl_users where ushauri_id = '$updated_by'")->result();
                    foreach ($updated_by_existence as $value) {
                        $new_updated_by .= $value->id;
                    }


                    // singlebyte strings
                    $clinic_no_mfl_code = substr($clinic_number, 0, 5);
                    // multibyte strings
                    $clinic_no_mfl_code = mb_substr($clinic_number, 0, 5);

//                    $check_client_existence2 = $this->db->query("Select id from tbl_client where mfl_code = '$clinic_no_mfl_code' and clinic_number = '$clinic_number' and f_name='$f_name' and m_name='$m_name' and l_name='$l_name' ");
                    $check_client_existence2 = $this->db->get_where('tbl_client', array('mfl_code' => $mfl_code, 'clinic_number' => $clinic_number));
                    $check_client_existence2_value = $check_client_existence2->num_rows();

                    if ($check_client_existence2_value > 0) {
                        echo 'Client Found in Both T4A and Ushauri ....<br>';
                        //Update the  record details.....
                        foreach ($check_client_existence2->result() as $client_value) {
                            echo 'Update transaction ';
                            $t4a_client_id = $client_value->id;
                            $this->db->trans_start();
                            $data_update = array(
                                'ushauri_id' => $id,
                                'db_source' => $db_source
                            );
                            $this->db->where('id', $t4a_client_id);
                            $this->db->update('client', $data_update);
                            $this->db->trans_complete();
                            if ($this->db->trans_status() == FALSE) {
                                
                            } else {
                                
                            }
                        }
                    } else {
                        echo 'Insert transaction ....';
                        // echo "Record was not found , kindly insert to DataBase ";
                        //Insert the  record details ....
//                        echo 'Grop ID => ' . $group_id . '  New Group ID =>' . $new_group_id . '<br>';
//                        echo 'Language ID => ' . $language_id . '  New Language ID =>' . $new_language_id . '<br>';
//                        echo 'Marital ID => ' . $marital . '  New Marital ID =>' . $new_marital_id . '<br>';
//            exit;
//                        echo 'Gender ID => ' . $gender . '  New Gender ID =>' . $new_gender_id . '<br>';

                        if ($new_gender_id == '11') {
                            $new_gender_id = '1';
                        } else {
                            
                        }
                        $this->db->trans_start();


                        $data = array(
                            'ushauri_id' => $id,
                            'group_id' => $new_group_id,
                            'language_id' => $new_language_id,
                            'facility_id' => $facility_id,
                            'clinic_number' => $clinic_number,
                            'f_name' => $f_name,
                            'm_name' => $m_name,
                            'l_name' => $l_name,
                            'status' => $status,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                            'created_at' => $created_at,
                            'updated_at' => $updated_at,
                            'dob' => $dob,
                            'client_status' => $client_status,
                            'txt_frequency' => $txt_frequency,
                            'txt_time' => $new_txt_time,
                            'phone_no' => $phone_no,
                            'alt_phone_no' => $alt_phone_no,
                            'shared_no_name' => $shared_no_name,
                            'smsenable' => $smsenable,
                            'partner_id' => $new_partner_id,
                            'mfl_code' => $mfl_code,
                            'gender' => $new_gender_id,
                            'marital' => $new_marital_id,
                            'enrollment_date' => $enrollment_date,
                            'art_date' => $art_date,
                            'wellness_enable' => $wellness_enable,
                            'motivational_enable' => $motivational_enable,
                            'client_type' => $client_type,
                            'prev_clinic' => $prev_clinic,
                            'transfer_date' => $transfer_date,
                            'entry_point' => $entry_point,
                            'welcome_sent' => $welcome_sent,
                            'stable' => $stable,
                            'physical_address' => $physical_address,
                            'consent_date' => $consent_date,
                            'db_source' => $db_source
                        );

                        $this->db->insert('tbl_client', $data);

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_partner_facility = $ushauri_db->query("Select * from tbl_client where mfl_code='$mfl_code' ")->result();
            foreach ($get_partner_facility as $value) {
                $status = $value->status;
                $id = $value->id;
                $group_id = $value->group_id;
                $language_id = $value->language_id;
                $facility_id = $value->facility_id;
                $clinic_number = $value->clinic_number;
                $f_name = $value->f_name;
                $m_name = $value->m_name;
                $l_name = $value->l_name;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;
                $dob = $value->dob;
                $client_status = $value->client_status;
                $txt_frequency = $value->txt_frequency;
                $txt_time = $value->txt_time;
                $phone_no = $value->phone_no;
                $alt_phone_no = $value->alt_phone_no;
                $shared_no_name = $value->shared_no_name;
                $smsenable = $value->smsenable;
                $partner_id = $value->partner_id;
                $mfl_code = $value->mfl_code;
                $gender = $value->gender;
                $marital = $value->marital;
                $enrollment_date = $value->enrollment_date;
                $art_date = $value->art_date;
                $wellness_enable = $value->wellness_enable;
                $motivational_enable = $value->motivational_enable;
                $client_type = $value->client_type;
                $prev_clinic = $value->prev_clinic;
                $transfer_date = $value->transfer_date;
                $entry_point = $value->entry_point;
                $welcome_sent = $value->welcome_sent;
                $stable = $value->stable;
                $physical_address = $value->physical_address;
                $consent_date = $value->consent_date;


                $new_group_id = '';
                $new_language_id = '';
                $new_facility_id = '';
                $new_txt_time = '';
                $new_partner_id = '';
                $new_gender_id = '';
                $new_marital_id = '';
                $new_created_by = '';
                $new_updated_by = '';

                $group_existence = $this->db->query("Select * from tbl_groups where ushauri_id = '$group_id'")->result();
                foreach ($group_existence as $value) {
                    $new_group_id .= $value->id;
                }
                if ($language_id == 5 or $language_id == 3) {
                    $new_language_id = '3';
                } else {
                    $language_existence = $this->db->query("Select * from tbl_language where ushauri_id = '$language_id'")->result();
                    foreach ($language_existence as $value) {
                        $new_language_id .= $value->id;
                    }
                }

                $txt_time_existence = $this->db->query("Select * from tbl_time where ushauri_id = '$txt_time'")->result();
                foreach ($txt_time_existence as $value) {
                    $new_txt_time .= $value->id;
                }
                $partner_existence = $this->db->query("Select * from tbl_partner where ushauri_id = '$partner_id'")->result();
                foreach ($partner_existence as $value) {
                    $new_partner_id .= $value->id;
                }
                $gender_existence = $this->db->query("Select * from tbl_gender where ushauri_id = '$gender'")->result();
                foreach ($group_existence as $value) {
                    $new_gender_id .= $value->id;
                }
                $marital_existence = $this->db->query("Select * from tbl_marital_status where ushauri_id = '$marital'")->result();
                foreach ($marital_existence as $value) {
                    $new_marital_id .= $value->id;
                }
                $created_by_existence = $this->db->query("Select * from tbl_users where ushauri_id = '$created_by'")->result();
                foreach ($created_by_existence as $value) {
                    $new_created_by .= $value->id;
                }
                $updated_by_existence = $this->db->query("Select * from tbl_users where ushauri_id = '$updated_by'")->result();
                foreach ($updated_by_existence as $value) {
                    $new_updated_by .= $value->id;
                }


                // singlebyte strings
                $clinic_no_mfl_code = substr($clinic_number, 0, 5);
                // multibyte strings
                $clinic_no_mfl_code = mb_substr($clinic_number, 0, 5);

                //  $check_client_existence2 = $this->db->query("Select id from tbl_client where mfl_code = '$clinic_no_mfl_code' and clinic_number = '$clinic_number' and f_name='$f_name' and m_name='$m_name' and l_name='$l_name' ");
                $check_client_existence2 = $this->db->get_where('tbl_client', array('mfl_code' => $clinic_no_mfl_code, 'f_name' => $f_name, 'm_name' => $m_name, 'l_name' => $l_name, 'clinic_number' => $clinic_number));
                if ($check_client_existence2->num_rows() > 0) {
                    echo 'Client Found in Both T4A and Ushauri ....<br>';
                    //Update the  record details.....
                    foreach ($check_client_existence2->result() as $client_value) {
                        $t4a_client_id = $client_value->id;
                        $this->db->trans_start();
                        $data_update = array(
                            'ushauri_id' => $id,
                            'db_source' => $db_source
                        );
                        $this->db->where('id', $t4a_client_id);
                        $this->db->update('client', $data_update);
                        $this->db->trans_complete();
                        if ($this->db->trans_start() == FALSE) {
                            
                        } else {
                            
                        }
                    }
                } else {
                    //Insert the  record details ....
//                    echo 'Grop ID => ' . $group_id . '  New Group ID =>' . $new_group_id . '<br>';
//                    echo 'Language ID => ' . $language_id . '  New Language ID =>' . $new_language_id . '<br>';
//                    echo 'Marital ID => ' . $marital . '  New Marital ID =>' . $new_marital_id . '<br>';
//            exit;

                    $this->db->trans_start();


                    $data = array(
                        'ushauri_id' => $id,
                        'group_id' => $new_group_id,
                        'language_id' => $new_language_id,
                        'facility_id' => $facility_id,
                        'clinic_number' => $clinic_number,
                        'f_name' => $f_name,
                        'm_name' => $m_name,
                        'l_name' => $l_name,
                        'status' => $status,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'dob' => $dob,
                        'client_status' => $client_status,
                        'txt_frequency' => $txt_frequency,
                        'txt_time' => $new_txt_time,
                        'phone_no' => $phone_no,
                        'alt_phone_no' => $alt_phone_no,
                        'shared_no_name' => $shared_no_name,
                        'smsenable' => $smsenable,
                        'partner_id' => $new_partner_id,
                        'mfl_code' => $mfl_code,
                        'gender' => $new_gender_id,
                        'marital' => $new_marital_id,
                        'enrollment_date' => $enrollment_date,
                        'art_date' => $art_date,
                        'wellness_enable' => $wellness_enable,
                        'motivational_enable' => $motivational_enable,
                        'client_type' => $client_type,
                        'prev_clinic' => $prev_clinic,
                        'transfer_date' => $transfer_date,
                        'entry_point' => $entry_point,
                        'welcome_sent' => $welcome_sent,
                        'stable' => $stable,
                        'physical_address' => $physical_address,
                        'consent_date' => $consent_date,
                        'db_source' => $db_source
                    );

                    $this->db->insert('tbl_client', $data);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        
                    }
                }
            }
        }
    }

    function appointment_types($db_source = null) {
        $post_ushauri_db = $this->load->database('post_T4A_Ushauri', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_last_insert_id = $post_ushauri_db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_appointment_types WHERE ushauri_id IS NOT NULL ");
        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                if (empty($last_insert_id)) {
                    $last_insert_id .= '0';
                } else {
                    
                }

                $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                $cleaned_appointment_types = $ushauri_db->query("SELECT DISTINCT app_type_1 as app_type_1  FROM tbl_appointment where id > $last_insert_id")->result();
                foreach ($cleaned_appointment_types as $value) {
                    $appointment_type = $value->app_type_1;
                    echo 'Appointment Type => ' . $appointment_type . '<br>';
                    $check_appointmet_existence = $post_ushauri_db->query("Select * from tbl_appointment_types where name = '$appointment_type'");
                    $num_rows = $check_appointmet_existence->num_rows();
                    echo $num_rows;
                    if ($num_rows > 0) {
                        echo 'Appointment Type exists < br> ';
                    } else if ($num_rows <= 0) {
                        echo 'Appointment Type does not exists < br> ';
                        foreach ($check_appointmet_existence->result() as $value) {
                            
                        }
                        $date = date('Y-m-d H:i:s');
//                    $appointment_type_name = $value->app_type_1;
                        $data_insert = array(
                            'name' => $appointment_type,
                            'created_at' => $date,
                            'created_by' => '1',
                            'updated_by' => '1',
                            'db_source' => $db_source
                        );
                        $post_ushauri_db->insert('tbl_appointment_types', $data_insert);
                    }
                }
            }
        } else {
            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $cleaned_appointment_types = $ushauri_db->query("SELECT DISTINCT app_type_1 as app_type_1  FROM tbl_appointment")->result();
            foreach ($cleaned_appointment_types as $value) {
                $appointment_type = $value->app_type_1;
                echo 'Appointment Type => ' . $appointment_type . '<br>';
                $check_appointmet_existence = $post_ushauri_db->query("Select * from tbl_appointment_types where name = '$appointment_type'");
                $num_rows = $check_appointmet_existence->num_rows();
                echo $num_rows;
                if ($num_rows > 0) {
                    echo 'Appointment Type exists < br> ';
                } else if ($num_rows <= 0) {
                    echo 'Appointment Type does not exists < br> ';
                    foreach ($check_appointmet_existence->result() as $value) {
                        
                    }
                    $date = date('Y-m-d H:i:s');
//                    $appointment_type_name = $value->app_type_1;
                    $data_insert = array(
                        'name' => $appointment_type,
                        'created_at' => $date,
                        'created_by' => '1',
                        'updated_by' => '1',
                        'db_source' => $db_source
                    );
                    $post_ushauri_db->insert('tbl_appointment_types', $data_insert);
                }
            }
        }
    }

    function appointments($db_source = null, $mfl_code = null) {
        $post_ushauri_db = $this->load->database('post_T4A_Ushauri', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.


        /*
         * 

         * Get all apppointments from ushauri 
         * Check for the new client id from the  t4a_ushauri table
         * Map the  new clients id for each new insertion
         * Check for the  appointment types from the  table appointment types table and insert it to the  new appointment table         */


        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_appointment "
                . " INNER JOIN tbl_client ON tbl_client.id = tbl_appointment.client_id  WHERE ushauri_id IS NOT NULL and tbl_client.mfl_code ='$mfl_code' ");
        $no_records = $get_last_insert_id->num_rows();
        echo $no_records;


        IF ($get_last_insert_id->num_rows() > 0) {
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                echo 'Appointment Last Insert ID =>' . $last_insert_id . '<br>';

                if ($last_insert_id == '0' or empty($last_insert_id)) {


                    $get_appointemnt_query = $ushauri_db->query(" Select * from tbl_appointment "
                                    . " inner join tbl_client on tbl_client.id = tbl_appointment.client_id "
                                    . " where tbl_appointment.id > '$last_insert_id' and tbl_client.mfl_code='$mfl_code'  ")->result();
                    //print_r($get_appointemnt_query);

                    foreach ($get_appointemnt_query as $value) {
                        $ushauri_id = $value->id;
                        $client_id = $value->client_id;
                        $appntmnt_date = $value->appntmnt_date;
                        $app_type_1 = $value->app_type_1;
                        $created_at = $value->created_at;
                        $updated_at = $value->updated_at;
                        $app_status = $value->app_status;
                        $app_msg = $value->app_msg;
                        $status = $value->status;
                        $notified = $value->notified;
                        $sent_status = $value->sent_status;
                        $created_by = $value->created_by;
                        $updated_by = $value->updated_by;
                        $entry_point = $value->entry_point;
                        $appointment_kept = $value->appointment_kept;
                        $active_app = $value->active_app;
                        $no_calls = $value->no_calls;
                        $no_msgs = $value->no_msgs;
                        $home_visits = $value->home_visits;
                        $visit_type = $value->visit_type;
                        $unscheduled_date = $value->unscheduled_date;
                        $tracer_name = $value->tracer_name;
                        $fnl_trcing_outocme = $value->fnl_trcing_outocme;
                        $fnl_outcome_dte = $value->fnl_outcome_dte;
                        $other_trcing_outcome = $value->other_trcing_outcome;

                        $new_appointment_type = $this->db->query("Select * from tbl_appointment_types where name like '%$app_type_1%'");
                        $check_existence = $new_appointment_type->num_rows();
                        if ($check_existence > 0) {
                            $results = $new_appointment_type->result();
                            foreach ($results as $value) {
                                $appointment_type_id = $value->id;
                                $new_client_query = $this->db->query("Select id from tbl_client where ushauri_id= '$client_id'");
                                foreach ($new_client_query->result() as $value) {
                                    $new_client_id = $value->id;


                                    $this->db->trans_start();

                                    $data_insert = array(
                                        'client_id' => $new_client_id,
                                        'appntmnt_date' => $appntmnt_date,
                                        'app_type_1' => $appointment_type_id,
                                        'created_at' => $created_at,
                                        'created_by' => $created_by,
                                        'updated_at' => $updated_at,
                                        'updated_by' => $updated_by,
                                        'app_status' => $app_status,
                                        'app_msg' => $app_msg,
                                        'status' => $status,
                                        'notified' => $notified,
                                        'sent_status' => $sent_status,
                                        'entry_point' => $entry_point,
                                        'appointment_kept' => $appointment_kept,
                                        'active_app' => $active_app,
                                        'no_calls' => $no_calls,
                                        'no_msgs' => $no_msgs,
                                        'home_visits' => $home_visits,
                                        'visit_type' => $visit_type,
                                        'unscheduled_date' => $unscheduled_date,
                                        'tracer_name' => $tracer_name,
                                        'fnl_trcing_outcome' => $fnl_trcing_outocme,
                                        'fnl_outcome_dte' => $fnl_outcome_dte,
                                        'other_trcing_outcome' => $other_trcing_outcome,
                                        'ushauri_id' => $ushauri_id,
                                        'db_source' => $db_source
                                    );

                                    $this->db->insert('appointment', $data_insert);
                                    $this->db->trans_complete();
                                    if ($this->db->trans_status() === FALSE) {
                                        
                                    } else {
                                        
                                    }
                                }
                            }
                        } else {
                            
                        }
                    }
                } else {


                    $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

                    $get_appointemnt_query = $ushauri_db->query(" Select * from tbl_appointment "
                                    . " inner join tbl_client on tbl_client.id = tbl_appointment.client_id "
                                    . " where 1 and tbl_client.mfl_code ='$mfl_code' id > $last_insert_id  ")->result();
                    foreach ($get_appointemnt_query as $value) {
                        $ushauri_id = $value->id;
                        $client_id = $value->client_id;
                        $appntmnt_date = $value->appntmnt_date;
                        $app_type_1 = $value->app_type_1;
                        $created_at = $value->created_at;
                        $updated_at = $value->updated_at;
                        $app_status = $value->app_status;
                        $app_msg = $value->app_msg;
                        $status = $value->status;
                        $notified = $value->notified;
                        $sent_status = $value->sent_status;
                        $created_by = $value->created_by;
                        $updated_by = $value->updated_by;
                        $entry_point = $value->entry_point;
                        $appointment_kept = $value->appointment_kept;
                        $active_app = $value->active_app;
                        $no_calls = $value->no_calls;
                        $no_msgs = $value->no_msgs;
                        $home_visits = $value->home_visits;
                        $visit_type = $value->visit_type;
                        $unscheduled_date = $value->unscheduled_date;
                        $tracer_name = $value->tracer_name;
                        $fnl_trcing_outocme = $value->fnl_trcing_outocme;
                        $fnl_outcome_dte = $value->fnl_outcome_dte;
                        $other_trcing_outcome = $value->other_trcing_outcome;

                        $new_appointment_type = $this->db->query("Select * from tbl_appointment_types where name like '%$app_type_1%'");
                        $check_existence = $new_appointment_type->num_rows();
                        if ($check_existence > 0) {
                            $results = $new_appointment_type->result();
                            foreach ($results as $value) {
                                $appointment_type_id = $value->id;
                                $new_client_query = $this->db->query("Select id from tbl_client where ushauri_id= '$client_id'");
                                foreach ($new_client_query->result() as $value) {
                                    $new_client_id = $value->id;


                                    $this->db->trans_start();

                                    $data_insert = array(
                                        'client_id' => $new_client_id,
                                        'appntmnt_date' => $appntmnt_date,
                                        'app_type_1' => $appointment_type_id,
                                        'created_at' => $created_at,
                                        'created_by' => $created_by,
                                        'updated_at' => $updated_at,
                                        'updated_by' => $updated_by,
                                        'app_status' => $app_status,
                                        'app_msg' => $app_msg,
                                        'status' => $status,
                                        'notified' => $notified,
                                        'sent_status' => $sent_status,
                                        'entry_point' => $entry_point,
                                        'appointment_kept' => $appointment_kept,
                                        'active_app' => $active_app,
                                        'no_calls' => $no_calls,
                                        'no_msgs' => $no_msgs,
                                        'home_visits' => $home_visits,
                                        'visit_type' => $visit_type,
                                        'unscheduled_date' => $unscheduled_date,
                                        'tracer_name' => $tracer_name,
                                        'fnl_trcing_outcome' => $fnl_trcing_outocme,
                                        'fnl_outcome_dte' => $fnl_outcome_dte,
                                        'other_trcing_outcome' => $other_trcing_outcome,
                                        'ushauri_id' => $ushauri_id,
                                        'db_source' => $db_source
                                    );

                                    $this->db->insert('appointment', $data_insert);
                                    $this->db->trans_complete();
                                    if ($this->db->trans_status() === FALSE) {
                                        
                                    } else {
                                        
                                    }
                                }
                            }
                        } else {
                            
                        }
                    }
                }
            }
        } else {

            $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

            $get_appointemnt_query = $ushauri_db->query(" Select * from tbl_appointment "
                            . " inner join tbl_client on tbl_client.id = tbl_appointment.client_id "
                            . "   ")->result();

            foreach ($get_appointemnt_query as $value) {
                $ushauri_id = $value->id;
                $client_id = $value->client_id;
                $appntmnt_date = $value->appntmnt_date;
                $app_type_1 = $value->app_type_1;
                $created_at = $value->created_at;
                $updated_at = $value->updated_at;
                $app_status = $value->app_status;
                $app_msg = $value->app_msg;
                $status = $value->status;
                $notified = $value->notified;
                $sent_status = $value->sent_status;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $entry_point = $value->entry_point;
                $appointment_kept = $value->appointment_kept;
                $active_app = $value->active_app;
                $no_calls = $value->no_calls;
                $no_msgs = $value->no_msgs;
                $home_visits = $value->home_visits;
                $visit_type = $value->visit_type;
                $unscheduled_date = $value->unscheduled_date;
                $tracer_name = $value->tracer_name;
                $fnl_trcing_outocme = $value->fnl_trcing_outocme;
                $fnl_outcome_dte = $value->fnl_outcome_dte;
                $other_trcing_outcome = $value->other_trcing_outcome;

                $new_appointment_type = $this->db->query("Select * from tbl_appointment_types where name like '%$app_type_1%'");
                $check_existence = $new_appointment_type->num_rows();
                if ($check_existence > 0) {
                    $results = $new_appointment_type->result();
                    foreach ($results as $value) {
                        $appointment_type_id = $value->id;
                        $new_client_query = $this->db->query("Select id from tbl_client where ushauri_id= '$client_id'");
                        foreach ($new_client_query->result() as $value) {
                            $new_client_id = $value->id;


                            $this->db->trans_start();

                            $data_insert = array(
                                'client_id' => $new_client_id,
                                'appntmnt_date' => $appntmnt_date,
                                'app_type_1' => $appointment_type_id,
                                'created_at' => $created_at,
                                'created_by' => $created_by,
                                'updated_at' => $updated_at,
                                'updated_by' => $updated_by,
                                'app_status' => $app_status,
                                'app_msg' => $app_msg,
                                'status' => $status,
                                'notified' => $notified,
                                'sent_status' => $sent_status,
                                'entry_point' => $entry_point,
                                'appointment_kept' => $appointment_kept,
                                'active_app' => $active_app,
                                'no_calls' => $no_calls,
                                'no_msgs' => $no_msgs,
                                'home_visits' => $home_visits,
                                'visit_type' => $visit_type,
                                'unscheduled_date' => $unscheduled_date,
                                'tracer_name' => $tracer_name,
                                'fnl_trcing_outcome' => $fnl_trcing_outocme,
                                'fnl_outcome_dte' => $fnl_outcome_dte,
                                'other_trcing_outcome' => $other_trcing_outcome,
                                'ushauri_id' => $ushauri_id,
                                'db_source' => $db_source
                            );

                            $this->db->insert('appointment', $data_insert);
                            $this->db->trans_complete();
                            if ($this->db->trans_status() === FALSE) {
                                
                            } else {
                                
                            }
                        }
                    }
                } else {
                    
                }
            }
        }
    }

    function clnt_outgoing_msgs($db_source = null, $mfl_code = null) {

        $post_ushauri_db = $this->load->database('post_T4A_Ushauri', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.


        $ushauri_db = $this->load->database($db_source, TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_last_insert_id = $this->db->query(" SELECT max(ushauri_id) as ushauri_id  FROM tbl_clnt_outgoing "
                . "	INNER JOIN tbl_client on tbl_client.id = tbl_clnt_outgoing.clnt_usr_id  "
                . " WHERE ushauri_id IS NOT NULL AND tbl_client.mfl_code='$mfl_code' ");
        IF ($get_last_insert_id->num_rows() > 0) {
            echo "Greater than 0 condition met .... ";
            foreach ($get_last_insert_id->result() as $value) {
                $last_insert_id = $value->ushauri_id;
                echo $last_insert_id;

                if ($last_insert_id == '0' or empty($last_insert_id)) {
                    echo 'Equals to 0 and not empty condition met. ';
                    $getclient_msgs = $ushauri_db->query("Select * from tbl_clnt_outgoing 	"
                                    . "INNER JOIN tbl_client on tbl_client.id = tbl_clnt_outgoing.clnt_usr_id "
                                    . " where 1  and id > $last_insert_id and tbl_client.mfl_code='$mfl_code' group by id   ")->result();
                    foreach ($getclient_msgs as $value) {
                        $ushauri_id = $value->id;
                        $destination = $value->destination;
                        $source = $value->source;
                        $msg = $value->msg;
                        $updated_at = $value->updated_at;
                        $created_at = $value->created_at;
                        $status = $value->status;
                        $responded = $value->responded;
                        $message_type_id = $value->message_type_id;
                        $content_id = $value->content_id;
                        $recepient_type = $value->recepient_type;
                        $created_by = $value->created_by;
                        $updated_by = $value->updated_by;
                        $is_deleted = $value->is_deleted;
                        $clnt_usr_id = $value->clnt_usr_id;


                        $new_client_query = $this->db->query("Select id from tbl_client where ushauri_id= '$clnt_usr_id'");
                        foreach ($new_client_query->result() as $value) {
                            $new_client_id = $value->id;
                            //echo "New Client ID => " . $new_client_id . "<br>";
                            $new_message_type = $this->db->query("Select * from tbl_message_types where ushauri_id='$message_type_id'")->result();
                            foreach ($new_message_type as $value2) {
                                $new_message_type_id = $value2->id;

                                $new_created_by = '';
                                $new_updated_by = '';

                                $new_created_by_qry = $this->db->query("Select id from tbl_users where ushauri_id = '$created_by'")->result();
                                foreach ($new_created_by_qry as $value) {
                                    $new_created_by .= $value->id;
                                }


                                $new_updated_by_qry = $this->db->query("Select id from tbl_users where ushauri_id = '$updated_by'")->result();
                                foreach ($new_updated_by_qry as $value) {
                                    $new_updated_by .= $value->id;
                                }
                                // echo "New Message ID => " . $new_message_type_id . "<br>";
                                $this->db->trans_start();
                                $data_insert = array(
                                    'ushauri_id' => $ushauri_id,
                                    'destination' => $destination,
                                    'source' => $source,
                                    'msg' => $msg,
                                    'updated_at' => $updated_at,
                                    'created_at' => $created_at,
                                    'status' => $status,
                                    'responded' => $responded,
                                    'message_type_id' => $new_message_type_id,
                                    'content_id' => $content_id,
                                    'recepient_type' => $recepient_type,
                                    'created_by' => $new_created_by,
                                    'updated_by' => $new_updated_by,
                                    'is_deleted' => $is_deleted,
                                    'clnt_usr_id' => $new_client_id,
                                    'db_source' => $db_source
                                );
                                $this->db->insert('tbl_clnt_outgoing', $data_insert);
                                $this->db->trans_complete();
                                if ($this->db->trans_status() === FALSE) {
                                    
                                } else {
                                    
                                }
                            }
                        }
                    }
                } else if ($last_insert_id > 0) {
                    echo "Greater than 0 for last insert ID has been met";
                    //exit;
                    $getclient_msgs = $ushauri_db->query("	Select * from tbl_clnt_outgoing 	"
                                    . "INNER JOIN tbl_client on tbl_client.id = tbl_clnt_outgoing.clnt_usr_id "
                                    . "  where id > $last_insert_id and tbl_client.mfl_code='$mfl_code'  order by id  ASC")->result();
                    foreach ($getclient_msgs as $value) {
                        $ushauri_id = $value->id;
                        $destination = $value->destination;
                        $source = $value->source;
                        $msg = $value->msg;
                        $updated_at = $value->updated_at;
                        $created_at = $value->created_at;
                        $status = $value->status;
                        $responded = $value->responded;
                        $message_type_id = $value->message_type_id;
                        $content_id = $value->content_id;
                        $recepient_type = $value->recepient_type;
                        $created_by = $value->created_by;
                        $updated_by = $value->updated_by;
                        $is_deleted = $value->is_deleted;
                        $clnt_usr_id = $value->clnt_usr_id;


                        $new_client_query = $this->db->query("Select id from tbl_client where ushauri_id= '$clnt_usr_id'");
                        foreach ($new_client_query->result() as $value) {
                            $new_client_id = $value->id;
                            // echo "New Client ID => " . $new_client_id . "<br>";
                            $new_message_type = $this->db->query("Select * from tbl_message_types where ushauri_id='$message_type_id'")->result();
                            foreach ($new_message_type as $value2) {
                                $new_message_type_id = $value2->id;

                                $new_created_by = '';
                                $new_updated_by = '';

                                $new_created_by_qry = $this->db->query("Select id from tbl_users where ushauri_id = '$created_by'")->result();
                                foreach ($new_created_by_qry as $value) {
                                    $new_created_by .= $value->id;
                                }


                                $new_updated_by_qry = $this->db->query("Select id from tbl_users where ushauri_id = '$updated_by'")->result();
                                foreach ($new_updated_by_qry as $value) {
                                    $new_updated_by .= $value->id;
                                }
                                // echo "New Message ID => " . $new_message_type_id . "<br>";
                                $this->db->trans_start();
                                $data_insert = array(
                                    'ushauri_id' => $ushauri_id,
                                    'destination' => $destination,
                                    'source' => $source,
                                    'msg' => $msg,
                                    'updated_at' => $updated_at,
                                    'created_at' => $created_at,
                                    'status' => $status,
                                    'responded' => $responded,
                                    'message_type_id' => $new_message_type_id,
                                    'content_id' => $content_id,
                                    'recepient_type' => $recepient_type,
                                    'created_by' => $new_created_by,
                                    'updated_by' => $new_updated_by,
                                    'is_deleted' => $is_deleted,
                                    'clnt_usr_id' => $new_client_id,
                                    'db_source' => $db_source
                                );
                                $this->db->insert('tbl_clnt_outgoing', $data_insert);
                                $this->db->trans_complete();
                                if ($this->db->trans_status() === FALSE) {
                                    
                                } else {
                                    
                                }
                            }
                        }
                    }
                }
            }
        } else {

            $getclient_msgs = $ushauri_db->query("Select * from tbl_clnt_outgoing "
                            . "	INNER JOIN tbl_client on tbl_client.id = tbl_clnt_outgoing.clnt_usr_id "
                            . "  where tbl_client.mfl_code = '$mfl_code' group by id    ")->result();
            foreach ($getclient_msgs as $value) {
                $ushauri_id = $value->id;
                $destination = $value->destination;
                $source = $value->source;
                $msg = $value->msg;
                $updated_at = $value->updated_at;
                $created_at = $value->created_at;
                $status = $value->status;
                $responded = $value->responded;
                $message_type_id = $value->message_type_id;
                $content_id = $value->content_id;
                $recepient_type = $value->recepient_type;
                $created_by = $value->created_by;
                $updated_by = $value->updated_by;
                $is_deleted = $value->is_deleted;
                $clnt_usr_id = $value->clnt_usr_id;


                $new_client_query = $this->db->query("Select id from tbl_client where ushauri_id= '$clnt_usr_id'");
                foreach ($new_client_query->result() as $value) {
                    $new_client_id = $value->id;
                    // echo "New Client ID => " . $new_client_id . "<br>";
                    $new_message_type = $this->db->query("Select * from tbl_message_types where ushauri_id='$message_type_id'")->result();
                    foreach ($new_message_type as $value2) {
                        $new_message_type_id = $value2->id;

                        $new_created_by = '';
                        $new_updated_by = '';

                        $new_created_by_qry = $this->db->query("Select id from tbl_users where ushauri_id = '$created_by'")->result();
                        foreach ($new_created_by_qry as $value) {
                            $new_created_by .= $value->id;
                        }


                        $new_updated_by_qry = $this->db->query("Select id from tbl_users where ushauri_id = '$updated_by'")->result();
                        foreach ($new_updated_by_qry as $value) {
                            $new_updated_by .= $value->id;
                        }
                        // echo "New Message ID => " . $new_message_type_id . "<br>";
                        $this->db->trans_start();
                        $data_insert = array(
                            'ushauri_id' => $ushauri_id,
                            'destination' => $destination,
                            'source' => $source,
                            'msg' => $msg,
                            'updated_at' => $updated_at,
                            'created_at' => $created_at,
                            'status' => $status,
                            'responded' => $responded,
                            'message_type_id' => $new_message_type_id,
                            'content_id' => $content_id,
                            'recepient_type' => $recepient_type,
                            'created_by' => $new_created_by,
                            'updated_by' => $new_updated_by,
                            'is_deleted' => $is_deleted,
                            'clnt_usr_id' => $new_client_id,
                            'db_source' => $db_source
                        );
                        $this->db->insert('tbl_clnt_outgoing', $data_insert);
                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            
                        }
                    }
                }
            }
        }
    }

}
